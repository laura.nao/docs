---
title: Grunt Chromebooks
---

`grunt` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

  - [HP Chromebook 11 G6 EE](https://support.hp.com/in-en/product/hp-chromebook-11-g6-ee/18280450/document/c05904799)
  - [Acer Chromebook
  315](https://www.acer.com/ac/en/US/press/2019/479116)
  - [Acer Chromebook 311 (C721)](https://9to5google.com/2019/01/23/acer-amd-chromebooks-education/)

The specs for these chromebooks vary in terms of display, connectivity,
devices and CPU, some of them using Intel 64 bit processors such as the
Celeron N2940 (Bay Trail) and the Celeron N3350 (Apollo Lake) and some
others using AMD 64 bit Stoney Ridge processors like the AMD A4-9120.

The Collabora LAVA lab contains the following `grunt` devices:

  - HP Chromebook 11 G6 EE:
    - [`hp-11A-G6-EE-grunt-cbg-0`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-0)
    - [`hp-11A-G6-EE-grunt-cbg-1`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-1)
    - [`hp-11A-G6-EE-grunt-cbg-2`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-2)
    - [`hp-11A-G6-EE-grunt-cbg-3`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-3)
    - [`hp-11A-G6-EE-grunt-cbg-4`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-4)
    - [`hp-11A-G6-EE-grunt-cbg-5`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-5)
    - [`hp-11A-G6-EE-grunt-cbg-6`](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-6)

### Debugging interfaces

`grunt` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the HP 14-DB0003na, the debug port is the USB-C port in the right
side (also used for the power supply).

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

  - When using a Servo v4 as a debugging interface, the Cr50 interface
    shuts down after issuing a `cold_reset:on` command. However, this
    doesn't seem to happen if the Servo v4 is connected to a power
    supply and charging the Chromebook.

  - In some setups the serial console gets stuck and freezes during one
    of the Coreboot stages. Closing and reopening the serial terminal
    recovers the problem, since the Chromebook keeps running. Most of
    the times this happens is at this point during the boot process:

   ```
   POST: 0x40
   agesawrapper_amdinitpost() entry
   DRAM clear on reset: Keep
   variant_mainboard_read_spd SPD index 9
   CBFS: 'Master Header Locator' located CBFS at [df0000:ffffc0)
   CBFS: Locating 'spd.bin'
   CBFS: Found @ offset 79bc0 size 2000
   ```
See also [Common issues](../common_issues.md).

### Example kernel command line arguments

```
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS2,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

the IP and path of the NFS share are examples and should be adapted to
the test setup.
