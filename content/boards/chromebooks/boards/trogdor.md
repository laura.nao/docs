---
title: Trogdor Chromebooks
---

`trogdor` is a board name for ARM-64 Chromebooks using an Qualcomm® Snapdragon™ 7c.

The `trogdor` Chromebook we're currently using in the lab is an [Acer Chromebook 511](https://www.acer.com/ac/fr/FR/content/series/acerchromebook511c741l)
(codename `limozeen`).

### Debugging interfaces

`Trogdor` boards have been flashed and tested with [Servo
v4](../../01-debugging_interfaces) interfaces.

The  supports CCD and can be debugged through
its USB-C port on the left side (power supply's one).

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

For this Chromebook, at the moment, only a ChromeOS kernel can be used
(not an upstream one).
Otherwise, see [Common issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

```
earlyprintk=ttyMSM0,115200n8 console=ttyMSM0,115200n8 root=/dev/ram0 ip=dhcp tftpserverip=<server_ip>
```
