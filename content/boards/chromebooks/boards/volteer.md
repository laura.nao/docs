---
title: Volteer Chromebooks
---

`volteer` is a board name for x86_64-based Chromebooks based on the
TigerLake architecture.


The Collabora LAVA lab currently contains the following `volteer` devices:

  - ASUS CX9400
    - [`asus-cx9400-volteer-cbg-0`](https://staging.lava.collabora.dev/scheduler/device/asus-cx9400-volteer-cbg-0)
      (in the staging instance).

### Debugging interfaces

`volteer` boards have been flashed and tested with both
[SuzyQ](../../01-debugging_interfaces) cables.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

```
console=ttyS0,115200n8 root=/dev/nfs ip=dhcp tftpserverip=10.154.97.199 ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

The IP and path of the NFS share are examples and should be adapted to
the test setup.
