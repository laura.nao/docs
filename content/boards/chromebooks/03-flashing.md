---
title: Flashing firmware on Chromebooks
weight: 3
---

Each Chromebook model in the lab uses a different type of firmware, but
they all have some commonalities.  They are all based on Coreboot with a
custom build of Depthcharge to enable serial console and run the
tftpboot command interactively.  They can all be flashed using a special
version of `flashrom` which is built with the Chromium OS SDK.  This
page explains how to flash firmware images on all the Chromebook models
present in the Collabora lab.


## Prerequisites

### Install servod packages

First of all, the `servod` tools needs to be installed as explained in
the [Chromebooks and Servo boards](../01-debugging_interfaces)
documentation.  Typically, all the LAVA rack dispatchers in the lab will
have these packages automatically installed by Chef.


### Install the firmware tools

To install the tools needed to flash the Chromebook firmware images
follow [these
instructions](https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/README.md#installing)
from the firmware tools README. This can be run on a dispatcher itself
in the lab, to be able to then flash Chromebook devices directly.


## Flashing a firmware image

Each Chromebook type requires a different firmware image to be flashed.
They are all currently all stored in
[images.collabora.co.uk](https://images.collabora.co.uk/lava/boot) and
need to be downloaded manually.

See the general [flashing
instructions](https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/README.md#flashing)
from the firmware tools README, and the specific ones for the Collabora
test lab below.

**NOTE:** Before flashing a Chromebook for the first time, always do a
backup first with `servoflash.py --backup`.

For example, to flash the firmware for a `rk3399-gru-kevin` device:

* SSH to the dispatcher where the device is attached and enter
  firmware-tools

  ```
  ssh lava-rack-cbg-2
  cd firmware-tools
  ```

* Download the firmware file

  ```
  wget https://images.collabora.co.uk/lava/boot/rk3399-gru-kevin/depthcharge-rk3399-gru-kevin-20180806.dev.bin
  ```

* Run the `servoflash.py` script with the names of the device to flash
  and the matching firmware file:

  ```
  ./servoflash.py \
    --device=rk3399-gru-kevin-cbg-0 \
    --firmware=depthcharge-rk3399-gru-kevin-20180806.dev.bin
  ```

It can take a few minutes.  There should be these messages around the
end, which can vary depending on the type of Chromebook:

```
Erasing and writing flash chip... Verifying flash... VERIFIED.
SUCCESS
```

Set the device status to "unknown" via the LAVA web interface or
`lavacli` and check that the healthcheck passes.
